@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>My Posts</h2>
                    </div>
                    <div class="panel-body">
                        <div class="text-right">
                            <a href="{{ route('posts.create') }}" class="btn btn-primary">+ Add New</a>
                        </div>
                    </div>
                </div>

                @foreach($posts as $post)
                    @include('posts._post')
                @endforeach

                <div class="text-center">
                    {!! $posts->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
