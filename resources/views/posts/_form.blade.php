<div class="form-group">
    {!! Form::label('Title *') !!}
    {!! Form::text('title', null, ['class'=>'form-control']) !!}
    @if ($errors->has('title'))
        <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('Body *') !!}
    {!! Form::textarea('body', null, ['class'=>'form-control']) !!}
    @if ($errors->has('body'))
        <span class="help-block">
            <strong>{{ $errors->first('body') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('Photo') !!}
    @if (isset($post))
        <div><img class="photo-small" src="/storage/{{ $post->photo }}"/></div>
    @endif
    {!! Form::file('photo', ['photo'=>'mimes:jpg,jpeg,bmp,png,gif|max:200']) !!}
    @if ($errors->has('photo'))
        <span class="help-block">
            <strong>{{ $errors->first('photo') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('Date posted') !!}
    {!! Form::input('date', 'published_at', isset($post) ? Carbon\Carbon::parse($post->published_at)->format('Y-m-d') : date('Y-m-d'), ['class'=>'form-control']) !!}
    @if ($errors->has('published_at'))
        <span class="help-block">
            <strong>{{ $errors->first('published_at') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
    <a href="{{ url()->previous() ? url()->previous() : route('posts.index') }}" class="btn btn-default">Cancel</a>
</div>