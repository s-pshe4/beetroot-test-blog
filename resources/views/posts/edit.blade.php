@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Edit Post</h3>
                    </div>

                    <div class="panel-body">

                        {!! Form::model($post, ['method'=>'PATCH','route'=>['posts.update', $post->id], 'files' => true]) !!}
                        @include('posts._form')
                        {!! Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
