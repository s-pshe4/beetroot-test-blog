<div class="panel panel-default">
    <div class="panel-heading">
        <h4>{{ $post->title }} <span class="published-at">| {{ Carbon\Carbon::parse($post->published_at)->format('d-m-Y, l') }} | {{ $post->author->name }}</span></h4>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                {!! nl2br($post->body) !!}
            </div>
            <div class="col-md-6">
                <img src="/storage/{{ $post->photo }}" class="post-photo">
            </div>
            @if ($post->user_id == Auth::id())
                <div class="col-md-12 text-right">
                    {!! Form::open(['url' => route('posts.destroy', $post->id), 'class' => 'inline-block', 'method' => 'delete']) !!}
                    {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-default">Edit</a>
                </div>
            @endif
        </div>
    </div>
</div>