<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the published posts.
     *
     * @param Post $postModel
     * @return \Illuminate\Http\Response
     */
    public function index(Post $postModel)
    {
        $posts = $postModel->getPublishedPosts();
        return view('home', compact('posts'));
    }
}
