<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogPostRequest;
use Auth;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the posts.
     *
     * @param Post $postModel
     * @return \Illuminate\Http\Response
     */
    public function index(Post $postModel)
    {
        $userId = Auth::id();
        $posts = $postModel->getUserPosts($userId);
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BlogPostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogPostRequest $request)
    {
        $post = new Post();
        $post->fill($request->all());
        $post->user_id = Auth::id();
        if ($request->photo) {
            $post->photo = $request->photo->store('posts');
        }
        $post->save();
        return redirect()->route('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        if (!$post || $post->user_id != Auth::id()) {
            return redirect()->route('home');
        }
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BlogPostRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogPostRequest $request, $id)
    {
        $post = Post::find($id);

        if (!$post || $post->user_id != Auth::id()) {
            return redirect()->route('home');
        }

        $post->fill($request->all());
        if ($request->photo) {
            Storage::delete($post->photo);
            $post->photo = $request->photo->store('posts');
        }
        $post->save();

        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if ($post && $post->user_id == Auth::id()) {
            Storage::delete($post->photo);
            $post->delete();
        }

        return redirect()->back();

    }
}
