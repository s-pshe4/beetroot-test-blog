<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'published_at',
    ];


    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'user_id',
        'photo',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'published_at',
    ];


    /**
     * Get list of the published posts
     *
     * @return mixed
     */
    public function getPublishedPosts()
    {
        return $this->published()
                    ->orderBy('published_at', 'desc')
                    ->paginate(5);
    }

    /**
     * Get all user posts
     *
     * @param $userId
     * @return mixed
     */
    public function getUserPosts($userId)
    {
        return $this->where('user_id', $userId)
                    ->orderBy('published_at', 'desc')
                    ->paginate(5);
    }

    /**
     * Scope for filter only already published posts by date
     *
     * @param $query
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }

    /**
     * Get post author profile
     *
     * @return mixed
     */
    public function author()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }
}
