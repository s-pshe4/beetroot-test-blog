## Install

- Run this command to work the image functionality >
php artisan storage:link
- Configure .env file
- Set folders permissions

## Task
Small Test using Laravel (30-60min):

	•	Create small blog
	•	Attach to BitBucket for code review
	•	Blog Fields (only 4)
	•	Blog Title
	•	Blog Body Text
	•	Photo
	•	Date Posted
	•	Display, Add, Edit and Delete Functions

## Functionality

- Auth / Register
- See list of posts from all users (date of publication for which has already come)
- Add new post
- See list of "My Posts" from main dropdown
- Edit / Update personal posts
- Delete personal posts

## Demo Video (0:02:20)
https://drive.google.com/open?id=1hYRj3mQOZV95EO0ISnqgFsSJqF6HpZEH


